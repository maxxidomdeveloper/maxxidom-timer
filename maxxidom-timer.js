/**
 *  maxxidom Timer v1.0.1
 *  Autor: Alexander Bechthold
 *  Date: 02.10.2017
 *  Update: 19.10.2017
 *  Licensed under the MIT license
 */


function MaxxidomTimer(options) {

    this.hours    = options.hours    || 0;
    this.minutes  = options.minutes  || 0;
    this.seconds  = options.seconds  || 0;
    this.func     = options.function || undefined;
    this.folder   = document.querySelector(options.folder) || undefined;

    var timer   = this;
    var hours   = this.hours;
    var minutes = this.minutes;
    var seconds = this.seconds;
    var minsec  = 59;

    if(this.folder !== undefined){

        var spanHours = document.createElement('span');
        var spanH = this.folder.appendChild(spanHours);
        spanHours.className = 'MaxxidomTimer-hours';

        var spanMinutes = document.createElement('span');
        var spanM = this.folder.appendChild(spanMinutes);
        spanMinutes.className = 'MaxxidomTimer-mins';

        var spanSeconds = document.createElement('span');
        var spanS = this.folder.appendChild(spanSeconds);
        spanSeconds.className = 'MaxxidomTimer-seconds';


        this.interval = function(){
            if(seconds === 0){
                if(minutes === 0){
                    if(hours === 0){
                        clearInterval(timer.timeInterval);
                        if(timer.func !== undefined) timer.sufunc();
                    } else {
                        hours--; minutes = minsec; seconds = minsec;
                        timer.showHours(hours);
                    }
                    timer.showMinutes(minutes);
                } else {
                    minutes--; seconds = minsec;
                    timer.showMinutes(minutes);
                }
            } else { seconds--; }
            timer.showSeconds(seconds);
        };
        this.timeInterval = setInterval(timer.interval, 1000);


        this.show        = function(h, m, s) {
            if(h < 10) h = "0" + h;
            if(m < 10) m = "0" + m;
            if(s < 10) s = "0" + s;
            spanH.innerHTML = h;
            spanM.innerHTML = m;
            spanS.innerHTML = s;
        };
        this.showHours   = function(h) {
            if(h < 10) h = "0" + h;
            spanH.innerHTML = h;
        };
        this.showMinutes = function(m) {
            if(m < 10) m = "0" + m;
            spanM.innerHTML = m;
        };
        this.showSeconds = function(s) {
            if(s < 10) s = "0" + s;
            spanS.innerHTML = s;
        };

        this.show(hours, minutes, seconds);

        this.sufunc = function() {
            return timer.func();
        };

    }

}